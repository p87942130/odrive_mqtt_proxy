# odrive_mqtt_proxy


cd odrive_mqtt_proxy/

python3 -m venv env

source env/bin/activate

pip install --upgrade pip

pip install odrive

pip install paho-mqtt

run odrivetool to find serial number

and put serial instead of "XXXSERIALNUMBE111"

adjust rate.

python3 odrive_mqtt_proxy.py --serial XXXSERIALNUMBE111 --rate 2.0