#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime, threading, time
import sys
import argparse
import paho.mqtt.client as paho
import odrive
from odrive.enums import *
import json


def read_odrive_axis_trap_traj_data(trap_traj_data):
	trap_traj = {"config": {"vel_limit": trap_traj_data.config.vel_limit,
							"accel_limit": trap_traj_data.config.accel_limit,
							"decel_limit": trap_traj_data.config.decel_limit,
							"A_per_css_": trap_traj_data.config.A_per_css}}
	return trap_traj

def read_odrive_axis_controller_data(controller_data):
	controller = {"config": {"control_mode": controller_data.config.control_mode, "vel_limit": controller_data.config.vel_limit},
				"pos_setpoint": controller_data.pos_setpoint,
				"vel_setpoint": controller_data.vel_setpoint,
				"current_setpoint": controller_data.current_setpoint}
	return controller

def read_odrive_axis_motor_data(motor_data):
	motor = {"current_control":{"Iq_measured": motor_data.current_control.Iq_measured, "Iq_setpoint": motor_data.current_control.Iq_setpoint,
							"v_current_control_integral_d": motor_data.current_control.v_current_control_integral_d,
							"v_current_control_integral_q": motor_data.current_control.v_current_control_integral_q},
							"config":{"current_lim": motor_data.config.current_lim}}
	return motor

def read_odrive_axis_encoder_data(encoder_data):
	encoder = {"pos_estimate":encoder_data.pos_estimate,
				"vel_estimate":encoder_data.vel_estimate,
				"is_ready":encoder_data.is_ready,
				"pos_cpr":encoder_data.pos_cpr,
				"config":{"use_index":encoder_data.config.use_index,
							"pre_calibrated":encoder_data.config.pre_calibrated,
							"idx_search_speed":encoder_data.config.idx_search_speed,
							"cpr":encoder_data.config.cpr}}
	return encoder

def read_odrive_axis_config_data(config_data):
	config = {"startup_motor_calibration": config_data.startup_motor_calibration,
				"startup_encoder_index_search": config_data.startup_encoder_index_search,
				"startup_encoder_offset_calibration": config_data.startup_encoder_offset_calibration,
				"startup_closed_loop_control": config_data.startup_closed_loop_control,
				"startup_sensorless_control": config_data.startup_sensorless_control,
				"ramp_up_time": config_data.ramp_up_time,
				"ramp_up_distance": config_data.ramp_up_distance,
				"spin_up_current": config_data.spin_up_current,
				"spin_up_acceleration": config_data.spin_up_acceleration,
				"spin_up_target_vel": config_data.spin_up_target_vel}
	return config

def read_odrive_axis_data(axis_data):
	axis  = {}
	axis["motor"] = read_odrive_axis_motor_data(axis_data.motor)
	axis["encoder"] = read_odrive_axis_encoder_data(axis_data.encoder)
	axis["trap_traj"] = read_odrive_axis_trap_traj_data(axis_data.trap_traj)
	axis["controller"] = read_odrive_axis_controller_data(axis_data.controller)
	axis["config"] = read_odrive_axis_config_data(axis_data.config)
	axis["error"] = axis_data.error
	axis["step_dir_active"] = axis_data.step_dir_active
	axis["current_state"] = axis_data.current_state
	axis["loop_counter"] = axis_data.loop_counter
	return axis

def read_odrive_data(odrive_data):
	payload = {"vbus_voltage": my_drive.vbus_voltage, "axis0": read_odrive_axis_data(odrive_data.axis0), "axis1":read_odrive_axis_data(odrive_data.axis1)}
	return payload


def subscribe_odrive(client, odrv0):
	client = subscribe_topics(client, odrv0, "axis0")
	client = subscribe_topics(client, odrv0, "axis1")
	client.subscribe("odrive/" + odrv0 + '/reboot')
	print("odrive/" + odrv0 + '/reboot')
	client.subscribe("odrive/" + odrv0 + '/save_configuration')
	client.subscribe("odrive/" + odrv0 + '/erase_configuration')
	return client

def subscribe_topics(client, odrv_serial, axis):
	client.subscribe("odrive/" + odrv_serial + '/' + axis + "/requested_state", qos=1)
	client.subscribe("odrive/" + odrv_serial + '/' + axis + "/controller/pos_setpoint", qos=1)
	client.subscribe("odrive/" + odrv_serial + '/' + axis + "/controller/vel_setpoint", qos=1)
	client.subscribe("odrive/" + odrv_serial + '/' + axis + "/controller/current_setpoint", qos=1)
	client.subscribe("odrive/" + odrv_serial + '/' + axis + "/controller/move_to_pos", qos=1)
	client.subscribe("odrive/" + odrv_serial + '/' + axis + "/controller/move_incremental", qos=1)
	client.subscribe("odrive/" + odrv_serial + '/' + axis + "/controller/config/vel_limit", qos=1)
	client.subscribe("odrive/" + odrv_serial + '/' + axis + "/controller/config/control_mode", qos=1)
	client.subscribe("odrive/" + odrv_serial + '/' + axis + "/motor/config/current_lim", qos=1)
	client.subscribe("odrive/" + odrv_serial + '/' + axis + "/trap_traj/config/vel_limit", qos=1)
	client.subscribe("odrive/" + odrv_serial + '/' + axis + "/trap_traj/config/accel_limit", qos=1)
	client.subscribe("odrive/" + odrv_serial + '/' + axis + "/trap_traj/config/decel_limit", qos=1)
	client.subscribe("odrive/" + odrv_serial + '/' + axis + "/trap_traj/config/A_per_css_", qos=1)
	return client


odrive0_connected = False

def odrive_callback():
	global runtime
	global my_drive
	global rate
	global client
	global odrive0_connected
	next_call = time.time()
	while runtime:
		next_call = next_call+rate;
		time.sleep(next_call - time.time())
		# time.sleep(rate)
		if odrive0_connected == False:
			my_drive = odrive.find_any(serial_number=odrive_serial)
			odrive0_connected = True
			print("Odrive reconnected 206933A5304B")
			next_call = time.time()
		else:
			payload = read_odrive_data(my_drive)
			print(payload)
			client.publish("odrive/" + odrive_serial + "/data", json.dumps(payload))

def on_subscribe(client, userdata, mid, granted_qos):
	print("Subscribed: "+str(mid)+" "+str(granted_qos))

def on_message(client, userdata, msg):
	global runtime
	global my_drive
	global odrive0_connected
	print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))
	# decode_odrv(msg.topic)
	subtopic = msg.topic.split("/")
	print(subtopic)
	if subtopic[1] == "206933A5304B":
		print("Odrive: " + subtopic[1])
		# decode_command(my_drive, subtopic)
		if subtopic[2] == "reboot":
			print("Rebooting 206933A5304B")
			odrive0_connected = False
			my_drive.reboot()

		else:
			decode_command(my_drive, subtopic, msg.payload)
	elif subtopic[1] == "XXX":
		pass
	# if msg.topic == "odrive/state":
	# 	runtime = False
	# elif msg.topic == ("odrive/" + odrive_serial + "/axis0/requested_state"):
	# 	print("REQUEST STATE")
	# elif msg.topic == ("odrive/" + odrive_serial + "/axis0/pos_setpoint"):
	# 	print("pos_setpoint ")
	# elif msg.topic == ("odrive/" + odrive_serial + "/axis0/vel_setpoint"):
		# print("vel_setpoint ")

def decode_command(drive, topic, payload):
	if topic[2] == "axis0":
		decode_axis(topic, drive.axis0, payload)
	elif subtopic[2] == "axis1":
		pass
	elif subtopic[2] == "save_configuration":
		drive.save_configuration()
	elif subtopic[2] == "erase_configuration":
		drive.erase_configuration()

def decode_axis_config(topic, config, payload):
	value = False
	if payload == "True":
		value = True
	else:
		value = False
	if topic[4] == "startup_motor_calibration":
		config.startup_motor_calibration = value
	if topic[4] == "startup_encoder_index_search":
		config.startup_encoder_index_search = value
	if topic[4] == "startup_encoder_offset_calibration":
		config.startup_encoder_offset_calibration = value
	if topic[4] == "startup_closed_loop_control":
		config.startup_closed_loop_control = value
	if topic[4] == "startup_sensorless_control":
		config.startup_sensorless_control = value

def decode_controller(topic, controller, payload):
	if topic[4] == "pos_setpoint":
		controller.pos_setpoint = float(payload)
	elif topic[4] == "vel_setpoint":
		controller.vel_setpoint = float(payload)
	elif topic[4] == "current_setpoint":
		controller.vel_setpoint = float(payload)
	elif topic[4] == "move_to_pos":
		controller.move_to_pos(float(payload))
	elif topic[4] == "move_incremental":
		values = json.load(payload)
		controller.move_incremental(value["displacement"], value["from_goal_point"])
	elif topic[4] == "config":
		if topic[5] == "vel_limit":
			controller.config.vel_limit = float(payload)
		elif topic[5] == "control_mode":
			controller.config.control_mode = int(payload)

def decode_motor(topic, motor, payload):
	if topic[4] == "config":
		if topic[5] == "current_lim":
			motor.config.current_lim = float(payload)

def decode_trap_traj(topic, trap_traj, payload):
	if topic[4] == "config":
		if topic[5] == "vel_limit":
			trap_traj.config.vel_limit = float(payload)
		if topic[5] == "accel_limit":
			trap_traj.config.accel_limit = float(payload)
		if topic[5] == "decel_limit":
			trap_traj.config.decel_limit = float(payload)
		if topic[5] == "A_per_css":
			trap_traj.config.A_per_css = float(payload)

def decode_axis(topic, axis, payload):
	if topic[3] == "requested_state":
		axis.requested_state = int(payload);
	elif topic[3] == "controller":
		decode_controller(topic, axis.controller, payload)
	elif subtopic[3] == "encoder":
		pass
	elif topic[3] == "motor":
		decode_motor(topic, axis.motor, payload)
	elif topic[3] == "trap_traj":
		decode_trap_traj(topic, axis.trap_traj, payload)
	elif topic[3] == "config":
		decode_axis_config(topic, axis.config , payload)
# # Instantiate the parser
parser = argparse.ArgumentParser(description='Parse arguments for MQTT server, data rate')
# # Required positional argument
parser.add_argument('--rate', type=float,
					help='Sampling frequency')
parser.add_argument('--serial', type=str,
					help='Odrive Serial number')
args = parser.parse_args()
print("Argument values:")
print("Rate: {}, Serial:{}".format(args.rate, args.serial))
rate = args.rate
odrive_serial = args.serial

# Find a connected ODrive (this will block until you connect one)
print("Finding an odrive...")
my_drive = odrive.find_any(serial_number=odrive_serial)
print("Found: " + odrive_serial)
# set_pos_setpoint(pos_setpoint: float, vel_feed_forward: float, current_feed_forward: float)
# set_vel_setpoint(vel_setpoint: float, current_feed_forward: float)
# set_current_setpoint(current_setpoint: float)
# move_to_pos(pos_setpoint: float)
# move_incremental(displacement: float, from_goal_point: bool)
# start_anticogging_calibration()
runtime = True
client = paho.Client()
client.on_subscribe = on_subscribe
client.on_message = on_message
client.connect("localhost", 1883)
client =  subscribe_odrive(client, odrive_serial)
timerThread = threading.Thread(target=odrive_callback)
timerThread.daemon = True
timerThread.start()
client.loop_forever()
